#! /usr/bin/env python

import sys
import time
import rospy
from sensor_msgs.msg import JointState
from dynamixel_workbench_msgs.srv import JointCommand, JointCommandRequest
from std_msgs.msg import Header


class OpenManipulatorMove(object):
    def __init__(self):
        rospy.loginfo("OpenManipulatorMove INIT...Please wait.")

        # We subscribe to the joint states to have info of the system

        self.joint_states_topic_name = 'joint_states'
        self._check_join_states_ready()
        sub = rospy.Subscriber(self.joint_states_topic_name,
                               JointState, self.joint_states_callback)

        # We start the Publisher for the positions of the joints
        self.goal_dynamixel_position_publisher = rospy.Publisher('goal_dynamixel_position',
                                                                 JointState,
                                                                 queue_size=1)
        self._check_publishers_connection()

        # Wait for the service client /joint_command to be running
        joint_command_service_name = "joint_command"
        rospy.wait_for_service(joint_command_service_name)
        # Create the connection to the service
        self.joint_command_service = rospy.ServiceProxy(
            joint_command_service_name, JointCommand)

        rospy.loginfo("OpenManipulatorMove Ready!")

    def joint_states_callback(self, msg):
        """
        rosmsg show sensor_msgs/JointState
            std_msgs/Header header
              uint32 seq
              time stamp
              string frame_id
            string[] name
            float64[] position
            float64[] velocity
            float64[] effort

        :param msg:
        :return:
        """
        self.joint_states_msg = msg

    def _check_join_states_ready(self):
        self.joint_states_msg = None
        rospy.logwarn("Waiting for " +
                      self.joint_states_topic_name+" to be READY...")
        while self.joint_states_msg is None and not rospy.is_shutdown():
            try:
                self.joint_states_msg = rospy.wait_for_message(
                    self.joint_states_topic_name, JointState, timeout=5.0)
                rospy.logwarn(
                    "Current "+self.joint_states_topic_name+" READY=>")

            except:
                rospy.logerr("Current "+self.joint_states_topic_name +
                             " not ready yet, retrying ")

    def _check_publishers_connection(self):
        """
        Checks that all the publishers are working
        :return:
        """
        rate = rospy.Rate(10)  # 10hz
        while self.goal_dynamixel_position_publisher.get_num_connections() == 0 and not rospy.is_shutdown():
            rospy.logdebug("No susbribers to goal_dynamixel_position_publisher yet so we wait and try again")
            try:
                rate.sleep()
            except rospy.ROSInterruptException:
                # This is to avoid error when world is rested, time when backwards.
                pass
        rospy.logdebug("goal_dynamixel_position_publisher Publisher Connected")

        rospy.logdebug("All Publishers READY")

    def move_all_joints(self, position_array):

        # rospy.logwarn("move_all_joints STARTED")
        # We check that the position array has the correct number of elements
        number_of_joints = len(self.joint_states_msg.name)
        print("len(position_array)"+str(len(position_array)))
        # We ar eonly insterested in the firts 7 joints
        JOINT_ARM_NUM = 7
        if len(position_array) >= JOINT_ARM_NUM:
            # if self.check_gripper_pos_safe(position_array[6]):
            new_joint_position = JointState()

            h = Header()
            # Note you need to call rospy.init_node() before this will work
            h.stamp = rospy.Time.now()
            h.frame_id = self.joint_states_msg.header.frame_id

            new_joint_position.header = h
            print(str(self.joint_states_msg))
            for index in range(JOINT_ARM_NUM):

                new_joint_position.name.append(
                    self.joint_states_msg.name[index])
                new_joint_position.position.append(position_array[index])

                # These values arent used, so they dont matter really
                # new_joint_position.velocity.append(self.joint_states_msg.velocity[index])
                # new_joint_position.effort.append(self.joint_states_msg.effort[index])

            # rospy.logwarn("PUBLISH STARTED")
            self.goal_dynamixel_position_publisher.publish(new_joint_position)
            # rospy.logwarn("PUBLISH FINISHED")
            # else:
            #     rospy.logerr("Gripper position NOT valid=" + str(position_array[6]))
        else:
            rospy.logerr(
                "The Array given doesnt have the correct length="+str(number_of_joints))

        # rospy.logwarn("move_all_joints FINISHED")

    def move_one_joint(self, joint_id, position, unit="rad"):
        """
        rossrv show dynamixel_workbench_msgs/JointCommand
            string unit
            uint8 id
            float32 goal_position
            ---
            bool result

        :param joint_id:
        :param position:
        :param units:
        :return:
        """
        joint_cmd_req = JointCommandRequest()
        joint_cmd_req.unit = unit
        joint_cmd_req.id = joint_id
        joint_cmd_req.goal_position = position

        if joint_id == 7:
            rospy.logwarn("CHECKING Gripper Value is safe?")
            if self.check_gripper_pos_safe(position):

                # Send through the connection the name of the object to be deleted by the service
                result = self.joint_command_service(joint_cmd_req)
                rospy.logwarn("move_one_joint went ok?="+str(result))
            else:
                rospy.logwarn("Gripper Value Not safe=" + str(position))
        else:
            # Send through the connection the name of the object to be deleted by the service
            result = self.joint_command_service(joint_cmd_req)
            rospy.logwarn("move_one_joint went ok?=" + str(result))

    def get_joint_names(self):
        return self.joint_states_msg.name

    def check_gripper_pos_safe(self, gripper_value):
        """
        We need to check that the gripper pos is -1.0 > position[6] > -3.14
        Otherwise it gets jammed
        :param gripper_value:
        :return:
        """
        return (-0.5 > gripper_value > -2.0)


def movement_sequence_test(movement_speed):

    openman_obj = OpenManipulatorMove()

    # PICK
    joint_position_init = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -1.9, 0, 0]
    joint_position_home = [0.006135923322290182, 0.13499031960964203, -1.9067381620407104, -0.03681553900241852, -1.323825478553772, 0.003067961661145091, -1.9, 0, 0]
    joint_position1 = [-0.0015339808305725455, 0.02454369328916073, -1.9726994037628174, -0.03834952041506767, -1.1857671737670898, 0.004601942375302315, -1.9, 0, 0]
    joint_position2 = [-0.0015339808305725455, 0.02454369328916073, -1.9726994037628174, -0.03834952041506767, -1.1857671737670898, 0.004601942375302315, -1.0, 0, 0]
    joint_position3 = [0.006135923322290182, 0.13499031960964203, -1.9067381620407104, -
                       0.03681553900241852, -1.323825478553772, 0.003067961661145091, -1.0, 0, 0]
    joint_position4 = [-0.4555923044681549, 0.37275734543800354, -1.5447187423706055, -
                       0.03834952041506767, -1.2563302516937256, 0.0015339808305725455, -1.0, 0, 0]
    joint_position5 = [-1.546252727508545, 0.4080389142036438, -2.0509324073791504, -
                       0.03834952041506767, -1.3744468688964844, 0.004601942375302315, -1.0, 0, 0]
    joint_position6 = [-1.4848934412002563, 0.3436117172241211, -2.2687575817108154, -
                       0.03681553900241852, -1.0906603336334229, 0.05062136799097061, -1.0, 0, 0]
    joint_position7 = [-1.4848934412002563, 0.3436117172241211, -2.2687575817108154, -
                       0.03681553900241852, -1.0906603336334229, 0.05062136799097061, -1.9, 0, 0]

    joint_position_sequence_nod = []    
    joint_position_sequence_nod.append(joint_position_init)
    joint_position_sequence_nod.append(joint_position_home)
    joint_position_sequence_nod.append(joint_position1)
    joint_position_sequence_nod.append(joint_position2)
    joint_position_sequence_nod.append(joint_position3)
    joint_position_sequence_nod.append(joint_position4)
    joint_position_sequence_nod.append(joint_position5)
    joint_position_sequence_nod.append(joint_position6)
    joint_position_sequence_nod.append(joint_position7)
    joint_position_sequence_nod.append(joint_position5)
    joint_position_sequence_nod.append(joint_position4)


    for joint_position_array in joint_position_sequence_nod:
        openman_obj.move_all_joints(joint_position_array)
        time.sleep(movement_speed)

def grasp_cube_demo(movement_speed):
    movement_sequence_test(movement_speed)


if __name__ == "__main__":
    rospy.init_node('move_openmanipulator_node', log_level=rospy.WARN)

    if len(sys.argv) < 2:
        print("usage: grasp_openmmanipulator.py movement_speed")
    else:
        movement_speed = float(sys.argv[1])
        rospy.logwarn("Movement Speed="+str(movement_speed))
        grasp_cube_demo(movement_speed=movement_speed)
