#!/usr/bin/env python

import rospy
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from std_msgs.msg import Header
import time

"""
rossrv show dynamixel_workbench_msgs/JointCommand
string unit
uint8 id
float32 goal_position
---
bool result
"""

"""
header: 
  seq: 102
  stamp: 
    secs: 0
    nsecs:         0
  frame_id: ''
joint_names: 
  - id_1
  - id_2
  - id_3
  - id_4
  - id_5
  - id_6
points: 
  - 
    positions: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    velocities: []
    accelerations: []
    effort: []
    time_from_start: 
      secs: 1
      nsecs:         0

"""

"""
std_msgs/Header header
  uint32 seq
  time stamp
  string frame_id
string[] joint_names
trajectory_msgs/JointTrajectoryPoint[] points
  float64[] positions
  float64[] velocities
  float64[] accelerations
  float64[] effort
  duration time_from_start

"""


# Tools for grasping

class PosControlSim(object):

    def __init__(self):
        
        self.arm_joint_names = [ "id_1",
                                "id_2",
                                "id_3",
                                "id_4",
                                "id_5",
                                "id_6"]

        self.gripper_joint_names = [ "id_7"]


        self.arm_pub = rospy.Publisher('/arm/command', JointTrajectory, queue_size=10)
        self.gripper_pub = rospy.Publisher('/gripper/command', JointTrajectory, queue_size=10)
    
    def move_arm(self, joints_array_pos):

        j_t = JointTrajectory()

        h = Header()
        h.stamp = rospy.Time.now()  # Note you need to call rospy.init_node() before this will work
        h.frame_id = ""
        j_t.header = h
        jt.joint_names = self.arm_joint_names

        j_t_p = JointTrajectoryPoint()
        assert len(joints_array_pos) == len(self.arm_joint_names), "Length Of Joint Array doenst match joint list= "+str(joints_array_pos)+","+str(self.arm_joint_names)
        j_t_p.positions = joints_array_pos
        d = rospy.Duration.from_sec(1.0)
        j_t_p.time_from_start = d

        self.arm_pub.publish(j_t)
    
    def move_gripper(self, joints_array_pos):

        j_t = JointTrajectory()

        h = Header()
        h.stamp = rospy.Time.now()  # Note you need to call rospy.init_node() before this will work
        h.frame_id = ""
        j_t.header = h
        jt.joint_names = self.gripper_joint_names

        j_t_p = JointTrajectoryPoint()
        assert len(joints_array_pos) == len(self.gripper_joint_names), "Length Of Joint Array doenst match joint list= "+str(joints_array_pos)+","+str(self.gripper_joint_names)
        j_t_p.positions = joints_array_pos
        d = rospy.Duration.from_sec(1.0)
        j_t_p.time_from_start = d

        self.gripper_pub.publish(j_t)


if __name__ == "__main__":

    rospy.init_node('object_finder', anonymous=True)
    cube_side = 0.04
    pos_sim = PosControlSim()
    # pos_sim.move_arm = [0.7, 0.7, 0.0, 0.0, 0.0, 0.0]
    pos_sim.move_gripper = [0.0]
    time.sleep(3)
    
