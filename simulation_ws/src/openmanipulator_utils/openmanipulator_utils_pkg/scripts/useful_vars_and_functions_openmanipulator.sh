#! /usr/bin/env bash

BLUETEXT="\033[1;34m"
REDTEXT="\033[1;31m"
WHITETEXT="\033[0;37m"
GREENTEXT="\033[1;32m"


function ros_env_setup {
    echo -e "$BLUETEXT\nSetting up ROS env"
    set +x
    source /opt/ros/noetic/setup.bash
    source /home/tgrip/TheConstruct/ros_playground/open_manipulator_v2_ws/devel/setup.bash
    alias rosenv="export | grep ROS"
    set -x
    echo -e "$BLUETEXT\nSetting up ROS env DONE "
    echo -e "\n$WHITETEXT"
}
