#! /usr/bin/env python

import rospy
from geometry_msgs.msg import Twist
import os

# velocity limits
VX_LIM = 0.15
WZ_LIM = 1.0

class VelocityLimit():
    def __init__(self):
        # cmd_vel Subscriber
        self.vel_sub = rospy.Subscriber('/cmd_vel', Twist, self.velCallback)
        self.cmd_vel = Twist()

    def velCallback(self, vel_msg):
        self.cmd_vel = vel_msg


if __name__=='__main__':
    
    # init node
    node_name = 'limit_cmd_vel'
    rospy.init_node(node_name)

    # declare rate for loop
    rate = rospy.Rate(2)

    # create VelocityLimit instance
    limit_vel = VelocityLimit()

    # loop
    while not rospy.is_shutdown():

        # check that cmd_vel is under limits
        if abs(limit_vel.cmd_vel.linear.x) > VX_LIM or abs(limit_vel.cmd_vel.angular.z) > WZ_LIM:
            print("cmd_vel published exceeds the established limits. Killing publisher")
            os.system("/home/apoc/catkin_ws/src/ignisbot_recovery_systems/ignisbot_manager/scripts/kill_cmd_vel_publisher_nodes.sh")
            print("BEFORE: " + str(limit_vel.cmd_vel))
            vel_pub = rospy.Publisher("/cmd_vel", Twist, queue_size=1)
            vel_pub.publish(Twist())
            print("AFTER: " + str(limit_vel.cmd_vel))
            #limit_vel.cmd_vel = Twist()
    
            
        else:
            pass

        rate.sleep()
