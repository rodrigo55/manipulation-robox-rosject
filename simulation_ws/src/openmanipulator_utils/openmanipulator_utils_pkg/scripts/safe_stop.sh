#! /usr/bin/env bash

function kil_all_goal_dynamixel_position_pubisher_nodes {
    echo -e "$BLUETEXT\n kil_all_goal_dynamixel_position_pubisher_nodes"
    set +x
   
    echo "Getting list of Publishers of goal_dynamixel_position..."
    INFO=$(rostopic info /goal_dynamixel_position)
    INFO2=$(echo $INFO | sed -e 's/.*Publishers:\(.*\)Subscribers.*/\1/')
    IFS=' ' read -r -a array <<< $INFO2

    new_array=()
    for value in "${array[@]}"
    do
	[[ ( $value != *"(http://"* ) && ( $value != *".sh"* ) && ( $value != *".py"* ) && ( $value != *"move_group_watcher"* )  && ( $value != *"dynamixel_action_server"* ) ]] && new_array+=($value)
    done
    array=("${new_array[@]}")
    unset new_array
	
    new_array=()
    for value in "${array[@]}"
    do
        [[ $value == *"/"* ]] && new_array+=($value)
    done
    
    array=("${new_array[@]}")
    unset new_array


    echo "Getting list of Publishers of goal_dynamixel_position...DONE"
    echo "${array[@]}"


    echo "Kill of goal_dynamixel_position publishers nodes..."
    for value in "${array[@]}"
    do
        rosnode kill $value &
    done
    echo "Kill of goal_dynamixel_position publishers nodes...DONE"

   
    echo "cleanup of rosnode..."
    # We input yes because sometimes it asks this user input
    yes 2>/dev/null | rosnode cleanup &
    echo "cleanup of rosnode...DONE"

    set -x
    echo -e "$BLUETEXT\n kil_all_goal_dynamixel_position_pubisher_nodes...DONE"
    echo -e "\n$WHITETEXT"
}

function kill_all_follow_joint_trajectory_pubisher_nodes {
    echo -e "$BLUETEXT\n kill_all_follow_joint_trajectory_pubisher_nodes"
    set +x
   
    echo "Getting list of Publishers of follow_joint_trajectory..."
    INFO=$(rostopic info /arm/follow_joint_trajectory/goal)
    INFO2=$(echo $INFO | sed -e 's/.*Publishers:\(.*\)Subscribers.*/\1/')
    IFS=' ' read -r -a array <<< $INFO2

    new_array=()
    for value in "${array[@]}"
    do
	[[ ( $value != *"(http://"* ) && ( $value != *".sh"* ) && ( $value != *".py"* ) && ( $value != *"run_traj_control_node"* ) ]] && new_array+=($value)
    done
    array=("${new_array[@]}")
    unset new_array
	
    new_array=()
    for value in "${array[@]}"
    do
        [[ $value == *"/"* ]] && new_array+=($value)
    done
    
    array=("${new_array[@]}")
    unset new_array


    echo "Getting list of Publishers of follow_joint_trajectory...DONE"
    echo "${array[@]}"


    echo "Kill of follow_joint_trajectory publishers nodes..."
    for value in "${array[@]}"
    do
        rosnode kill $value &
    done
    echo "Kill of follow_joint_trajectory publishers nodes...DONE"

   
    echo "cleanup of rosnode..."
    # We input yes because sometimes it asks this user input
    yes 2>/dev/null | rosnode cleanup &
    echo "cleanup of rosnode...DONE"

    set -x
    echo -e "$BLUETEXT\n kill_all_follow_joint_trajectory_pubisher_nodes...DONE"
    echo -e "\n$WHITETEXT"
}

function publish_safe_joint {

    # We stop the head
    source /opt/ros/noetic/setup.bash
    # rostopic pub /cmd_vel sensor_msgs/JointState "{linear:{x: 0.0, y: 0.0, z: 0.0}, angular:{x: 0.0, y: 0.0, z: 0.0}}" --once
    echo "PublishSafePos..."
    rostopic pub /goal_dynamixel_position sensor_msgs/JointState "{header:{seq: 0,stamp:{secs: 0, nsecs: 0},frame_id: ''},name: ['id_1','id_2','id_3','id_4','id_5','id_6','id_7'],position: [0,0,0,0,0,0,0],velocity: [],effort: []}" --once &
    echo "PublishSafePos...DONE"

}

function main { 
    source /opt/ros/noetic/setup.bash
    source /home/theconstruct/catkin_ws/devel/setup.bash
    # source /home/tgrip/TheConstruct/ros_playground/open_manipulator_v2_ws/devel/setup.bash
    kil_all_goal_dynamixel_position_pubisher_nodes
    kill_all_follow_joint_trajectory_pubisher_nodes
    publish_safe_joint
}

main
