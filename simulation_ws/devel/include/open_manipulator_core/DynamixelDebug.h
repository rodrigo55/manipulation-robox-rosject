// Generated by gencpp from file open_manipulator_core/DynamixelDebug.msg
// DO NOT EDIT!


#ifndef OPEN_MANIPULATOR_CORE_MESSAGE_DYNAMIXELDEBUG_H
#define OPEN_MANIPULATOR_CORE_MESSAGE_DYNAMIXELDEBUG_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace open_manipulator_core
{
template <class ContainerAllocator>
struct DynamixelDebug_
{
  typedef DynamixelDebug_<ContainerAllocator> Type;

  DynamixelDebug_()
    : dxl_id()
    , present_temp()
    , present_load()
    , present_volt()
    , present_current()
    , present_pos()
    , present_vel()
    , goal_pos()
    , goal_vel()
    , return_delay_time()
    , feedforward_1st_gain()
    , feedforward_2nd_gain()
    , error_status()
    , temp_limit()
    , pos_p_gain()
    , pos_i_gain()
    , pos_d_gain()
    , vel_p_gain()
    , vel_i_gain()  {
    }
  DynamixelDebug_(const ContainerAllocator& _alloc)
    : dxl_id(_alloc)
    , present_temp(_alloc)
    , present_load(_alloc)
    , present_volt(_alloc)
    , present_current(_alloc)
    , present_pos(_alloc)
    , present_vel(_alloc)
    , goal_pos(_alloc)
    , goal_vel(_alloc)
    , return_delay_time(_alloc)
    , feedforward_1st_gain(_alloc)
    , feedforward_2nd_gain(_alloc)
    , error_status(_alloc)
    , temp_limit(_alloc)
    , pos_p_gain(_alloc)
    , pos_i_gain(_alloc)
    , pos_d_gain(_alloc)
    , vel_p_gain(_alloc)
    , vel_i_gain(_alloc)  {
  (void)_alloc;
    }



   typedef std::vector<int32_t, typename ContainerAllocator::template rebind<int32_t>::other >  _dxl_id_type;
  _dxl_id_type dxl_id;

   typedef std::vector<int32_t, typename ContainerAllocator::template rebind<int32_t>::other >  _present_temp_type;
  _present_temp_type present_temp;

   typedef std::vector<int32_t, typename ContainerAllocator::template rebind<int32_t>::other >  _present_load_type;
  _present_load_type present_load;

   typedef std::vector<int32_t, typename ContainerAllocator::template rebind<int32_t>::other >  _present_volt_type;
  _present_volt_type present_volt;

   typedef std::vector<int32_t, typename ContainerAllocator::template rebind<int32_t>::other >  _present_current_type;
  _present_current_type present_current;

   typedef std::vector<int32_t, typename ContainerAllocator::template rebind<int32_t>::other >  _present_pos_type;
  _present_pos_type present_pos;

   typedef std::vector<int32_t, typename ContainerAllocator::template rebind<int32_t>::other >  _present_vel_type;
  _present_vel_type present_vel;

   typedef std::vector<int32_t, typename ContainerAllocator::template rebind<int32_t>::other >  _goal_pos_type;
  _goal_pos_type goal_pos;

   typedef std::vector<int32_t, typename ContainerAllocator::template rebind<int32_t>::other >  _goal_vel_type;
  _goal_vel_type goal_vel;

   typedef std::vector<int32_t, typename ContainerAllocator::template rebind<int32_t>::other >  _return_delay_time_type;
  _return_delay_time_type return_delay_time;

   typedef std::vector<int32_t, typename ContainerAllocator::template rebind<int32_t>::other >  _feedforward_1st_gain_type;
  _feedforward_1st_gain_type feedforward_1st_gain;

   typedef std::vector<int32_t, typename ContainerAllocator::template rebind<int32_t>::other >  _feedforward_2nd_gain_type;
  _feedforward_2nd_gain_type feedforward_2nd_gain;

   typedef std::vector<int32_t, typename ContainerAllocator::template rebind<int32_t>::other >  _error_status_type;
  _error_status_type error_status;

   typedef std::vector<int32_t, typename ContainerAllocator::template rebind<int32_t>::other >  _temp_limit_type;
  _temp_limit_type temp_limit;

   typedef std::vector<int32_t, typename ContainerAllocator::template rebind<int32_t>::other >  _pos_p_gain_type;
  _pos_p_gain_type pos_p_gain;

   typedef std::vector<int32_t, typename ContainerAllocator::template rebind<int32_t>::other >  _pos_i_gain_type;
  _pos_i_gain_type pos_i_gain;

   typedef std::vector<int32_t, typename ContainerAllocator::template rebind<int32_t>::other >  _pos_d_gain_type;
  _pos_d_gain_type pos_d_gain;

   typedef std::vector<int32_t, typename ContainerAllocator::template rebind<int32_t>::other >  _vel_p_gain_type;
  _vel_p_gain_type vel_p_gain;

   typedef std::vector<int32_t, typename ContainerAllocator::template rebind<int32_t>::other >  _vel_i_gain_type;
  _vel_i_gain_type vel_i_gain;





  typedef boost::shared_ptr< ::open_manipulator_core::DynamixelDebug_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::open_manipulator_core::DynamixelDebug_<ContainerAllocator> const> ConstPtr;

}; // struct DynamixelDebug_

typedef ::open_manipulator_core::DynamixelDebug_<std::allocator<void> > DynamixelDebug;

typedef boost::shared_ptr< ::open_manipulator_core::DynamixelDebug > DynamixelDebugPtr;
typedef boost::shared_ptr< ::open_manipulator_core::DynamixelDebug const> DynamixelDebugConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::open_manipulator_core::DynamixelDebug_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::open_manipulator_core::DynamixelDebug_<ContainerAllocator> >::stream(s, "", v);
return s;
}


template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator==(const ::open_manipulator_core::DynamixelDebug_<ContainerAllocator1> & lhs, const ::open_manipulator_core::DynamixelDebug_<ContainerAllocator2> & rhs)
{
  return lhs.dxl_id == rhs.dxl_id &&
    lhs.present_temp == rhs.present_temp &&
    lhs.present_load == rhs.present_load &&
    lhs.present_volt == rhs.present_volt &&
    lhs.present_current == rhs.present_current &&
    lhs.present_pos == rhs.present_pos &&
    lhs.present_vel == rhs.present_vel &&
    lhs.goal_pos == rhs.goal_pos &&
    lhs.goal_vel == rhs.goal_vel &&
    lhs.return_delay_time == rhs.return_delay_time &&
    lhs.feedforward_1st_gain == rhs.feedforward_1st_gain &&
    lhs.feedforward_2nd_gain == rhs.feedforward_2nd_gain &&
    lhs.error_status == rhs.error_status &&
    lhs.temp_limit == rhs.temp_limit &&
    lhs.pos_p_gain == rhs.pos_p_gain &&
    lhs.pos_i_gain == rhs.pos_i_gain &&
    lhs.pos_d_gain == rhs.pos_d_gain &&
    lhs.vel_p_gain == rhs.vel_p_gain &&
    lhs.vel_i_gain == rhs.vel_i_gain;
}

template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator!=(const ::open_manipulator_core::DynamixelDebug_<ContainerAllocator1> & lhs, const ::open_manipulator_core::DynamixelDebug_<ContainerAllocator2> & rhs)
{
  return !(lhs == rhs);
}


} // namespace open_manipulator_core

namespace ros
{
namespace message_traits
{





template <class ContainerAllocator>
struct IsMessage< ::open_manipulator_core::DynamixelDebug_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::open_manipulator_core::DynamixelDebug_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::open_manipulator_core::DynamixelDebug_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::open_manipulator_core::DynamixelDebug_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::open_manipulator_core::DynamixelDebug_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::open_manipulator_core::DynamixelDebug_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::open_manipulator_core::DynamixelDebug_<ContainerAllocator> >
{
  static const char* value()
  {
    return "76c78346f1f2d00f38d1ed19974ced54";
  }

  static const char* value(const ::open_manipulator_core::DynamixelDebug_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x76c78346f1f2d00fULL;
  static const uint64_t static_value2 = 0x38d1ed19974ced54ULL;
};

template<class ContainerAllocator>
struct DataType< ::open_manipulator_core::DynamixelDebug_<ContainerAllocator> >
{
  static const char* value()
  {
    return "open_manipulator_core/DynamixelDebug";
  }

  static const char* value(const ::open_manipulator_core::DynamixelDebug_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::open_manipulator_core::DynamixelDebug_<ContainerAllocator> >
{
  static const char* value()
  {
    return "int32[] dxl_id\n"
"int32[] present_temp\n"
"int32[] present_load\n"
"int32[] present_volt\n"
"int32[] present_current\n"
"int32[] present_pos\n"
"int32[] present_vel\n"
"int32[] goal_pos\n"
"int32[] goal_vel\n"
"int32[] return_delay_time\n"
"int32[] feedforward_1st_gain\n"
"int32[] feedforward_2nd_gain\n"
"int32[] error_status\n"
"int32[] temp_limit\n"
"int32[] pos_p_gain\n"
"int32[] pos_i_gain\n"
"int32[] pos_d_gain\n"
"int32[] vel_p_gain\n"
"int32[] vel_i_gain\n"
;
  }

  static const char* value(const ::open_manipulator_core::DynamixelDebug_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::open_manipulator_core::DynamixelDebug_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.dxl_id);
      stream.next(m.present_temp);
      stream.next(m.present_load);
      stream.next(m.present_volt);
      stream.next(m.present_current);
      stream.next(m.present_pos);
      stream.next(m.present_vel);
      stream.next(m.goal_pos);
      stream.next(m.goal_vel);
      stream.next(m.return_delay_time);
      stream.next(m.feedforward_1st_gain);
      stream.next(m.feedforward_2nd_gain);
      stream.next(m.error_status);
      stream.next(m.temp_limit);
      stream.next(m.pos_p_gain);
      stream.next(m.pos_i_gain);
      stream.next(m.pos_d_gain);
      stream.next(m.vel_p_gain);
      stream.next(m.vel_i_gain);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct DynamixelDebug_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::open_manipulator_core::DynamixelDebug_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::open_manipulator_core::DynamixelDebug_<ContainerAllocator>& v)
  {
    s << indent << "dxl_id[]" << std::endl;
    for (size_t i = 0; i < v.dxl_id.size(); ++i)
    {
      s << indent << "  dxl_id[" << i << "]: ";
      Printer<int32_t>::stream(s, indent + "  ", v.dxl_id[i]);
    }
    s << indent << "present_temp[]" << std::endl;
    for (size_t i = 0; i < v.present_temp.size(); ++i)
    {
      s << indent << "  present_temp[" << i << "]: ";
      Printer<int32_t>::stream(s, indent + "  ", v.present_temp[i]);
    }
    s << indent << "present_load[]" << std::endl;
    for (size_t i = 0; i < v.present_load.size(); ++i)
    {
      s << indent << "  present_load[" << i << "]: ";
      Printer<int32_t>::stream(s, indent + "  ", v.present_load[i]);
    }
    s << indent << "present_volt[]" << std::endl;
    for (size_t i = 0; i < v.present_volt.size(); ++i)
    {
      s << indent << "  present_volt[" << i << "]: ";
      Printer<int32_t>::stream(s, indent + "  ", v.present_volt[i]);
    }
    s << indent << "present_current[]" << std::endl;
    for (size_t i = 0; i < v.present_current.size(); ++i)
    {
      s << indent << "  present_current[" << i << "]: ";
      Printer<int32_t>::stream(s, indent + "  ", v.present_current[i]);
    }
    s << indent << "present_pos[]" << std::endl;
    for (size_t i = 0; i < v.present_pos.size(); ++i)
    {
      s << indent << "  present_pos[" << i << "]: ";
      Printer<int32_t>::stream(s, indent + "  ", v.present_pos[i]);
    }
    s << indent << "present_vel[]" << std::endl;
    for (size_t i = 0; i < v.present_vel.size(); ++i)
    {
      s << indent << "  present_vel[" << i << "]: ";
      Printer<int32_t>::stream(s, indent + "  ", v.present_vel[i]);
    }
    s << indent << "goal_pos[]" << std::endl;
    for (size_t i = 0; i < v.goal_pos.size(); ++i)
    {
      s << indent << "  goal_pos[" << i << "]: ";
      Printer<int32_t>::stream(s, indent + "  ", v.goal_pos[i]);
    }
    s << indent << "goal_vel[]" << std::endl;
    for (size_t i = 0; i < v.goal_vel.size(); ++i)
    {
      s << indent << "  goal_vel[" << i << "]: ";
      Printer<int32_t>::stream(s, indent + "  ", v.goal_vel[i]);
    }
    s << indent << "return_delay_time[]" << std::endl;
    for (size_t i = 0; i < v.return_delay_time.size(); ++i)
    {
      s << indent << "  return_delay_time[" << i << "]: ";
      Printer<int32_t>::stream(s, indent + "  ", v.return_delay_time[i]);
    }
    s << indent << "feedforward_1st_gain[]" << std::endl;
    for (size_t i = 0; i < v.feedforward_1st_gain.size(); ++i)
    {
      s << indent << "  feedforward_1st_gain[" << i << "]: ";
      Printer<int32_t>::stream(s, indent + "  ", v.feedforward_1st_gain[i]);
    }
    s << indent << "feedforward_2nd_gain[]" << std::endl;
    for (size_t i = 0; i < v.feedforward_2nd_gain.size(); ++i)
    {
      s << indent << "  feedforward_2nd_gain[" << i << "]: ";
      Printer<int32_t>::stream(s, indent + "  ", v.feedforward_2nd_gain[i]);
    }
    s << indent << "error_status[]" << std::endl;
    for (size_t i = 0; i < v.error_status.size(); ++i)
    {
      s << indent << "  error_status[" << i << "]: ";
      Printer<int32_t>::stream(s, indent + "  ", v.error_status[i]);
    }
    s << indent << "temp_limit[]" << std::endl;
    for (size_t i = 0; i < v.temp_limit.size(); ++i)
    {
      s << indent << "  temp_limit[" << i << "]: ";
      Printer<int32_t>::stream(s, indent + "  ", v.temp_limit[i]);
    }
    s << indent << "pos_p_gain[]" << std::endl;
    for (size_t i = 0; i < v.pos_p_gain.size(); ++i)
    {
      s << indent << "  pos_p_gain[" << i << "]: ";
      Printer<int32_t>::stream(s, indent + "  ", v.pos_p_gain[i]);
    }
    s << indent << "pos_i_gain[]" << std::endl;
    for (size_t i = 0; i < v.pos_i_gain.size(); ++i)
    {
      s << indent << "  pos_i_gain[" << i << "]: ";
      Printer<int32_t>::stream(s, indent + "  ", v.pos_i_gain[i]);
    }
    s << indent << "pos_d_gain[]" << std::endl;
    for (size_t i = 0; i < v.pos_d_gain.size(); ++i)
    {
      s << indent << "  pos_d_gain[" << i << "]: ";
      Printer<int32_t>::stream(s, indent + "  ", v.pos_d_gain[i]);
    }
    s << indent << "vel_p_gain[]" << std::endl;
    for (size_t i = 0; i < v.vel_p_gain.size(); ++i)
    {
      s << indent << "  vel_p_gain[" << i << "]: ";
      Printer<int32_t>::stream(s, indent + "  ", v.vel_p_gain[i]);
    }
    s << indent << "vel_i_gain[]" << std::endl;
    for (size_t i = 0; i < v.vel_i_gain.size(); ++i)
    {
      s << indent << "  vel_i_gain[" << i << "]: ";
      Printer<int32_t>::stream(s, indent + "  ", v.vel_i_gain[i]);
    }
  }
};

} // namespace message_operations
} // namespace ros

#endif // OPEN_MANIPULATOR_CORE_MESSAGE_DYNAMIXELDEBUG_H
