; Auto-generated. Do not edit!


(cl:in-package my_moveit_planner-srv)


;//! \htmlinclude JointStatesValidationServiceMessage-request.msg.html

(cl:defclass <JointStatesValidationServiceMessage-request> (roslisp-msg-protocol:ros-message)
  ((joint_states_data
    :reader joint_states_data
    :initarg :joint_states_data
    :type sensor_msgs-msg:JointState
    :initform (cl:make-instance 'sensor_msgs-msg:JointState)))
)

(cl:defclass JointStatesValidationServiceMessage-request (<JointStatesValidationServiceMessage-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <JointStatesValidationServiceMessage-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'JointStatesValidationServiceMessage-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name my_moveit_planner-srv:<JointStatesValidationServiceMessage-request> is deprecated: use my_moveit_planner-srv:JointStatesValidationServiceMessage-request instead.")))

(cl:ensure-generic-function 'joint_states_data-val :lambda-list '(m))
(cl:defmethod joint_states_data-val ((m <JointStatesValidationServiceMessage-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader my_moveit_planner-srv:joint_states_data-val is deprecated.  Use my_moveit_planner-srv:joint_states_data instead.")
  (joint_states_data m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <JointStatesValidationServiceMessage-request>) ostream)
  "Serializes a message object of type '<JointStatesValidationServiceMessage-request>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'joint_states_data) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <JointStatesValidationServiceMessage-request>) istream)
  "Deserializes a message object of type '<JointStatesValidationServiceMessage-request>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'joint_states_data) istream)
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<JointStatesValidationServiceMessage-request>)))
  "Returns string type for a service object of type '<JointStatesValidationServiceMessage-request>"
  "my_moveit_planner/JointStatesValidationServiceMessageRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'JointStatesValidationServiceMessage-request)))
  "Returns string type for a service object of type 'JointStatesValidationServiceMessage-request"
  "my_moveit_planner/JointStatesValidationServiceMessageRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<JointStatesValidationServiceMessage-request>)))
  "Returns md5sum for a message object of type '<JointStatesValidationServiceMessage-request>"
  "e87b4cc5d856e87ecb6530fa9e8eb54a")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'JointStatesValidationServiceMessage-request)))
  "Returns md5sum for a message object of type 'JointStatesValidationServiceMessage-request"
  "e87b4cc5d856e87ecb6530fa9e8eb54a")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<JointStatesValidationServiceMessage-request>)))
  "Returns full string definition for message of type '<JointStatesValidationServiceMessage-request>"
  (cl:format cl:nil "sensor_msgs/JointState joint_states_data       # Joints states to be validated~%~%================================================================================~%MSG: sensor_msgs/JointState~%# This is a message that holds data to describe the state of a set of torque controlled joints. ~%#~%# The state of each joint (revolute or prismatic) is defined by:~%#  * the position of the joint (rad or m),~%#  * the velocity of the joint (rad/s or m/s) and ~%#  * the effort that is applied in the joint (Nm or N).~%#~%# Each joint is uniquely identified by its name~%# The header specifies the time at which the joint states were recorded. All the joint states~%# in one message have to be recorded at the same time.~%#~%# This message consists of a multiple arrays, one for each part of the joint state. ~%# The goal is to make each of the fields optional. When e.g. your joints have no~%# effort associated with them, you can leave the effort array empty. ~%#~%# All arrays in this message should have the same size, or be empty.~%# This is the only way to uniquely associate the joint name with the correct~%# states.~%~%~%Header header~%~%string[] name~%float64[] position~%float64[] velocity~%float64[] effort~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'JointStatesValidationServiceMessage-request)))
  "Returns full string definition for message of type 'JointStatesValidationServiceMessage-request"
  (cl:format cl:nil "sensor_msgs/JointState joint_states_data       # Joints states to be validated~%~%================================================================================~%MSG: sensor_msgs/JointState~%# This is a message that holds data to describe the state of a set of torque controlled joints. ~%#~%# The state of each joint (revolute or prismatic) is defined by:~%#  * the position of the joint (rad or m),~%#  * the velocity of the joint (rad/s or m/s) and ~%#  * the effort that is applied in the joint (Nm or N).~%#~%# Each joint is uniquely identified by its name~%# The header specifies the time at which the joint states were recorded. All the joint states~%# in one message have to be recorded at the same time.~%#~%# This message consists of a multiple arrays, one for each part of the joint state. ~%# The goal is to make each of the fields optional. When e.g. your joints have no~%# effort associated with them, you can leave the effort array empty. ~%#~%# All arrays in this message should have the same size, or be empty.~%# This is the only way to uniquely associate the joint name with the correct~%# states.~%~%~%Header header~%~%string[] name~%float64[] position~%float64[] velocity~%float64[] effort~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <JointStatesValidationServiceMessage-request>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'joint_states_data))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <JointStatesValidationServiceMessage-request>))
  "Converts a ROS message object to a list"
  (cl:list 'JointStatesValidationServiceMessage-request
    (cl:cons ':joint_states_data (joint_states_data msg))
))
;//! \htmlinclude JointStatesValidationServiceMessage-response.msg.html

(cl:defclass <JointStatesValidationServiceMessage-response> (roslisp-msg-protocol:ros-message)
  ((valid
    :reader valid
    :initarg :valid
    :type cl:boolean
    :initform cl:nil)
   (status_message
    :reader status_message
    :initarg :status_message
    :type cl:string
    :initform ""))
)

(cl:defclass JointStatesValidationServiceMessage-response (<JointStatesValidationServiceMessage-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <JointStatesValidationServiceMessage-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'JointStatesValidationServiceMessage-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name my_moveit_planner-srv:<JointStatesValidationServiceMessage-response> is deprecated: use my_moveit_planner-srv:JointStatesValidationServiceMessage-response instead.")))

(cl:ensure-generic-function 'valid-val :lambda-list '(m))
(cl:defmethod valid-val ((m <JointStatesValidationServiceMessage-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader my_moveit_planner-srv:valid-val is deprecated.  Use my_moveit_planner-srv:valid instead.")
  (valid m))

(cl:ensure-generic-function 'status_message-val :lambda-list '(m))
(cl:defmethod status_message-val ((m <JointStatesValidationServiceMessage-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader my_moveit_planner-srv:status_message-val is deprecated.  Use my_moveit_planner-srv:status_message instead.")
  (status_message m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <JointStatesValidationServiceMessage-response>) ostream)
  "Serializes a message object of type '<JointStatesValidationServiceMessage-response>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'valid) 1 0)) ostream)
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'status_message))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'status_message))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <JointStatesValidationServiceMessage-response>) istream)
  "Deserializes a message object of type '<JointStatesValidationServiceMessage-response>"
    (cl:setf (cl:slot-value msg 'valid) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'status_message) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'status_message) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<JointStatesValidationServiceMessage-response>)))
  "Returns string type for a service object of type '<JointStatesValidationServiceMessage-response>"
  "my_moveit_planner/JointStatesValidationServiceMessageResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'JointStatesValidationServiceMessage-response)))
  "Returns string type for a service object of type 'JointStatesValidationServiceMessage-response"
  "my_moveit_planner/JointStatesValidationServiceMessageResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<JointStatesValidationServiceMessage-response>)))
  "Returns md5sum for a message object of type '<JointStatesValidationServiceMessage-response>"
  "e87b4cc5d856e87ecb6530fa9e8eb54a")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'JointStatesValidationServiceMessage-response)))
  "Returns md5sum for a message object of type 'JointStatesValidationServiceMessage-response"
  "e87b4cc5d856e87ecb6530fa9e8eb54a")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<JointStatesValidationServiceMessage-response>)))
  "Returns full string definition for message of type '<JointStatesValidationServiceMessage-response>"
  (cl:format cl:nil "bool valid         # States if the requested JointStates are valid~%string status_message # Used to give details~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'JointStatesValidationServiceMessage-response)))
  "Returns full string definition for message of type 'JointStatesValidationServiceMessage-response"
  (cl:format cl:nil "bool valid         # States if the requested JointStates are valid~%string status_message # Used to give details~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <JointStatesValidationServiceMessage-response>))
  (cl:+ 0
     1
     4 (cl:length (cl:slot-value msg 'status_message))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <JointStatesValidationServiceMessage-response>))
  "Converts a ROS message object to a list"
  (cl:list 'JointStatesValidationServiceMessage-response
    (cl:cons ':valid (valid msg))
    (cl:cons ':status_message (status_message msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'JointStatesValidationServiceMessage)))
  'JointStatesValidationServiceMessage-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'JointStatesValidationServiceMessage)))
  'JointStatesValidationServiceMessage-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'JointStatesValidationServiceMessage)))
  "Returns string type for a service object of type '<JointStatesValidationServiceMessage>"
  "my_moveit_planner/JointStatesValidationServiceMessage")