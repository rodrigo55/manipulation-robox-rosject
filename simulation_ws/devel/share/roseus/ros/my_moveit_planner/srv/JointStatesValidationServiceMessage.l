;; Auto-generated. Do not edit!


(when (boundp 'my_moveit_planner::JointStatesValidationServiceMessage)
  (if (not (find-package "MY_MOVEIT_PLANNER"))
    (make-package "MY_MOVEIT_PLANNER"))
  (shadow 'JointStatesValidationServiceMessage (find-package "MY_MOVEIT_PLANNER")))
(unless (find-package "MY_MOVEIT_PLANNER::JOINTSTATESVALIDATIONSERVICEMESSAGE")
  (make-package "MY_MOVEIT_PLANNER::JOINTSTATESVALIDATIONSERVICEMESSAGE"))
(unless (find-package "MY_MOVEIT_PLANNER::JOINTSTATESVALIDATIONSERVICEMESSAGEREQUEST")
  (make-package "MY_MOVEIT_PLANNER::JOINTSTATESVALIDATIONSERVICEMESSAGEREQUEST"))
(unless (find-package "MY_MOVEIT_PLANNER::JOINTSTATESVALIDATIONSERVICEMESSAGERESPONSE")
  (make-package "MY_MOVEIT_PLANNER::JOINTSTATESVALIDATIONSERVICEMESSAGERESPONSE"))

(in-package "ROS")

(if (not (find-package "SENSOR_MSGS"))
  (ros::roseus-add-msgs "sensor_msgs"))




(defclass my_moveit_planner::JointStatesValidationServiceMessageRequest
  :super ros::object
  :slots (_joint_states_data ))

(defmethod my_moveit_planner::JointStatesValidationServiceMessageRequest
  (:init
   (&key
    ((:joint_states_data __joint_states_data) (instance sensor_msgs::JointState :init))
    )
   (send-super :init)
   (setq _joint_states_data __joint_states_data)
   self)
  (:joint_states_data
   (&rest __joint_states_data)
   (if (keywordp (car __joint_states_data))
       (send* _joint_states_data __joint_states_data)
     (progn
       (if __joint_states_data (setq _joint_states_data (car __joint_states_data)))
       _joint_states_data)))
  (:serialization-length
   ()
   (+
    ;; sensor_msgs/JointState _joint_states_data
    (send _joint_states_data :serialization-length)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; sensor_msgs/JointState _joint_states_data
       (send _joint_states_data :serialize s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; sensor_msgs/JointState _joint_states_data
     (send _joint_states_data :deserialize buf ptr-) (incf ptr- (send _joint_states_data :serialization-length))
   ;;
   self)
  )

(defclass my_moveit_planner::JointStatesValidationServiceMessageResponse
  :super ros::object
  :slots (_valid _status_message ))

(defmethod my_moveit_planner::JointStatesValidationServiceMessageResponse
  (:init
   (&key
    ((:valid __valid) nil)
    ((:status_message __status_message) "")
    )
   (send-super :init)
   (setq _valid __valid)
   (setq _status_message (string __status_message))
   self)
  (:valid
   (&optional (__valid :null))
   (if (not (eq __valid :null)) (setq _valid __valid)) _valid)
  (:status_message
   (&optional __status_message)
   (if __status_message (setq _status_message __status_message)) _status_message)
  (:serialization-length
   ()
   (+
    ;; bool _valid
    1
    ;; string _status_message
    4 (length _status_message)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _valid
       (if _valid (write-byte -1 s) (write-byte 0 s))
     ;; string _status_message
       (write-long (length _status_message) s) (princ _status_message s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _valid
     (setq _valid (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; string _status_message
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _status_message (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(defclass my_moveit_planner::JointStatesValidationServiceMessage
  :super ros::object
  :slots ())

(setf (get my_moveit_planner::JointStatesValidationServiceMessage :md5sum-) "e87b4cc5d856e87ecb6530fa9e8eb54a")
(setf (get my_moveit_planner::JointStatesValidationServiceMessage :datatype-) "my_moveit_planner/JointStatesValidationServiceMessage")
(setf (get my_moveit_planner::JointStatesValidationServiceMessage :request) my_moveit_planner::JointStatesValidationServiceMessageRequest)
(setf (get my_moveit_planner::JointStatesValidationServiceMessage :response) my_moveit_planner::JointStatesValidationServiceMessageResponse)

(defmethod my_moveit_planner::JointStatesValidationServiceMessageRequest
  (:response () (instance my_moveit_planner::JointStatesValidationServiceMessageResponse :init)))

(setf (get my_moveit_planner::JointStatesValidationServiceMessageRequest :md5sum-) "e87b4cc5d856e87ecb6530fa9e8eb54a")
(setf (get my_moveit_planner::JointStatesValidationServiceMessageRequest :datatype-) "my_moveit_planner/JointStatesValidationServiceMessageRequest")
(setf (get my_moveit_planner::JointStatesValidationServiceMessageRequest :definition-)
      "sensor_msgs/JointState joint_states_data       # Joints states to be validated

================================================================================
MSG: sensor_msgs/JointState
# This is a message that holds data to describe the state of a set of torque controlled joints. 
#
# The state of each joint (revolute or prismatic) is defined by:
#  * the position of the joint (rad or m),
#  * the velocity of the joint (rad/s or m/s) and 
#  * the effort that is applied in the joint (Nm or N).
#
# Each joint is uniquely identified by its name
# The header specifies the time at which the joint states were recorded. All the joint states
# in one message have to be recorded at the same time.
#
# This message consists of a multiple arrays, one for each part of the joint state. 
# The goal is to make each of the fields optional. When e.g. your joints have no
# effort associated with them, you can leave the effort array empty. 
#
# All arrays in this message should have the same size, or be empty.
# This is the only way to uniquely associate the joint name with the correct
# states.


Header header

string[] name
float64[] position
float64[] velocity
float64[] effort

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id
---
bool valid         # States if the requested JointStates are valid
string status_message # Used to give details
")

(setf (get my_moveit_planner::JointStatesValidationServiceMessageResponse :md5sum-) "e87b4cc5d856e87ecb6530fa9e8eb54a")
(setf (get my_moveit_planner::JointStatesValidationServiceMessageResponse :datatype-) "my_moveit_planner/JointStatesValidationServiceMessageResponse")
(setf (get my_moveit_planner::JointStatesValidationServiceMessageResponse :definition-)
      "sensor_msgs/JointState joint_states_data       # Joints states to be validated

================================================================================
MSG: sensor_msgs/JointState
# This is a message that holds data to describe the state of a set of torque controlled joints. 
#
# The state of each joint (revolute or prismatic) is defined by:
#  * the position of the joint (rad or m),
#  * the velocity of the joint (rad/s or m/s) and 
#  * the effort that is applied in the joint (Nm or N).
#
# Each joint is uniquely identified by its name
# The header specifies the time at which the joint states were recorded. All the joint states
# in one message have to be recorded at the same time.
#
# This message consists of a multiple arrays, one for each part of the joint state. 
# The goal is to make each of the fields optional. When e.g. your joints have no
# effort associated with them, you can leave the effort array empty. 
#
# All arrays in this message should have the same size, or be empty.
# This is the only way to uniquely associate the joint name with the correct
# states.


Header header

string[] name
float64[] position
float64[] velocity
float64[] effort

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id
---
bool valid         # States if the requested JointStates are valid
string status_message # Used to give details
")



(provide :my_moveit_planner/JointStatesValidationServiceMessage "e87b4cc5d856e87ecb6530fa9e8eb54a")


