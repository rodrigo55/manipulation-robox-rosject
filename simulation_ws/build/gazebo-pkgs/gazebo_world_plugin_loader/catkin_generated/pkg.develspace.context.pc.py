# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/user/simulation_ws/src/gazebo-pkgs/gazebo_world_plugin_loader/include".split(';') if "/home/user/simulation_ws/src/gazebo-pkgs/gazebo_world_plugin_loader/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;gazebo_ros;gazebo_ros".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lgazebo_world_plugin_loader".split(';') if "-lgazebo_world_plugin_loader" != "" else []
PROJECT_NAME = "gazebo_world_plugin_loader"
PROJECT_SPACE_DIR = "/home/user/simulation_ws/devel"
PROJECT_VERSION = "1.0.2"
