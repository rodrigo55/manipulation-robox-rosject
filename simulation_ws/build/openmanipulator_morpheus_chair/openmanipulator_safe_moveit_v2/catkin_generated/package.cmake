set(_CATKIN_CURRENT_PACKAGE "openmanipulator_safe_moveit_v2")
set(openmanipulator_safe_moveit_v2_VERSION "0.3.0")
set(openmanipulator_safe_moveit_v2_MAINTAINER "RDaneelOlivaw <duckfrost@theconstruct.com>")
set(openmanipulator_safe_moveit_v2_PACKAGE_FORMAT "1")
set(openmanipulator_safe_moveit_v2_BUILD_DEPENDS )
set(openmanipulator_safe_moveit_v2_BUILD_EXPORT_DEPENDS "moveit_ros_move_group" "moveit_fake_controller_manager" "moveit_kinematics" "moveit_planners_ompl" "moveit_ros_visualization" "moveit_setup_assistant" "joint_state_publisher" "joint_state_publisher_gui" "robot_state_publisher" "tf2_ros" "xacro" "open_manipulator_support_description")
set(openmanipulator_safe_moveit_v2_BUILDTOOL_DEPENDS "catkin")
set(openmanipulator_safe_moveit_v2_BUILDTOOL_EXPORT_DEPENDS )
set(openmanipulator_safe_moveit_v2_EXEC_DEPENDS "moveit_ros_move_group" "moveit_fake_controller_manager" "moveit_kinematics" "moveit_planners_ompl" "moveit_ros_visualization" "moveit_setup_assistant" "joint_state_publisher" "joint_state_publisher_gui" "robot_state_publisher" "tf2_ros" "xacro" "open_manipulator_support_description")
set(openmanipulator_safe_moveit_v2_RUN_DEPENDS "moveit_ros_move_group" "moveit_fake_controller_manager" "moveit_kinematics" "moveit_planners_ompl" "moveit_ros_visualization" "moveit_setup_assistant" "joint_state_publisher" "joint_state_publisher_gui" "robot_state_publisher" "tf2_ros" "xacro" "open_manipulator_support_description")
set(openmanipulator_safe_moveit_v2_TEST_DEPENDS )
set(openmanipulator_safe_moveit_v2_DOC_DEPENDS )
set(openmanipulator_safe_moveit_v2_URL_WEBSITE "http://moveit.ros.org/")
set(openmanipulator_safe_moveit_v2_URL_BUGTRACKER "https://github.com/ros-planning/moveit/issues")
set(openmanipulator_safe_moveit_v2_URL_REPOSITORY "https://github.com/ros-planning/moveit")
set(openmanipulator_safe_moveit_v2_DEPRECATED "")