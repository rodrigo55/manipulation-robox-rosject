# generated from catkin/cmake/em/order_packages.cmake.em

set(CATKIN_ORDERED_PACKAGES "")
set(CATKIN_ORDERED_PACKAGE_PATHS "")
set(CATKIN_ORDERED_PACKAGES_IS_META "")
set(CATKIN_ORDERED_PACKAGES_BUILD_TYPE "")
list(APPEND CATKIN_ORDERED_PACKAGES "dynamixel_workbench")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "dynamixel-workbench/dynamixel_workbench")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "True")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "mimic_joint_gazebo_tutorial")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "mimic_joint_gazebo_tutorial")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "om_moveit_config")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "open_manipulator_controls/om_moveit_config")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "open_manipulator_controls")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "open_manipulator_controls/open_manipulator_controls")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "True")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "open_manipulator_moveit_config")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "open_manipulator_controls/open_manipulator_moveit_config")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "openmanipulator_movit_config")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "open_manipulator_tc/openmanipulator_movit_config")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "openmanipulator_safe_moveit")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "openmanipulator_morpheus_chair/openmanipulator_safe_moveit")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "openmanipulator_safe_moveit_v2")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "openmanipulator_morpheus_chair/openmanipulator_safe_moveit_v2")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "realsense2_description")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "realsense-ros/realsense2_description")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "dynamixel_workbench_msgs")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "dynamixel-workbench-msgs/dynamixel_workbench_msgs")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "dynamixel_position_controller")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "open_manipulator_tc/dynamixel_position_controller")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "dynamixel_workbench_toolbox")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "dynamixel-workbench/dynamixel_workbench_toolbox")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "openmanipulator_utils_pkg")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "openmanipulator_utils/openmanipulator_utils_pkg")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "assem_urdf10_description")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "open_manipulator_tc/assem_urdf10_description")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "open_manipulator_hw")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "open_manipulator_controls/open_manipulator_hw")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "path_navigation_msgs")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "general-message-pkgs/path_navigation_msgs")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "dodo_detector_ros")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "perception_object_detection/dodo_detector_ros")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "dynamixel_workbench_controllers")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "dynamixel-workbench/dynamixel_workbench_controllers")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "dynamixel_workbench_operators")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "dynamixel-workbench/dynamixel_workbench_operators")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "extended_object_detection")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "perception_object_detection/extended_object_detection")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "object_msgs")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "general-message-pkgs/object_msgs")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "openmanipulator_morpheus_chair_tutorials")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "openmanipulator_morpheus_chair/openmanipulator_morpheus_chair_tutorials")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "openmanipulator_trajectory_controller")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "open_manipulator_tc/openmanipulator_trajectory_controller")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "gazebo_test_tools")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "gazebo-pkgs/gazebo_test_tools")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "gazebo_version_helpers")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "gazebo-pkgs/gazebo_version_helpers")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "gazebo_grasp_plugin")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "gazebo-pkgs/gazebo_grasp_plugin")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "gazebo_world_plugin_loader")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "gazebo-pkgs/gazebo_world_plugin_loader")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "object_msgs_tools")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "general-message-pkgs/object_msgs_tools")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "gazebo_state_plugins")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "gazebo-pkgs/gazebo_state_plugins")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "realrobotlab")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "realrobotlab_simulation/realrobotlab")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "realsense2_camera")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "realsense-ros/realsense2_camera")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "realsense_gazebo_plugin")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "realsense_gazebo_plugin")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "roboticsgroup_gazebo_plugins")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "roboticsgroup_gazebo_plugins")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "surface_perception")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "perception_object_detection/surface_perception")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "open_manipulator_controllers")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "open_manipulator_controls/open_manipulator_controllers")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "assem_moveit_config")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "open_manipulator_tc/assem_moveit_config")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "open_manipulator_support_description")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "open_manipulator_tc/open_manipulator_support_description")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "my_moveit_planner")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "openmanipulator_utils/my_moveit_planner")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")
list(APPEND CATKIN_ORDERED_PACKAGES "open_manipulator_core")
list(APPEND CATKIN_ORDERED_PACKAGE_PATHS "open_manipulator_tc/open_manipulator_core")
list(APPEND CATKIN_ORDERED_PACKAGES_IS_META "False")
list(APPEND CATKIN_ORDERED_PACKAGES_BUILD_TYPE "catkin")

set(CATKIN_MESSAGE_GENERATORS )

set(CATKIN_METAPACKAGE_CMAKE_TEMPLATE "/usr/lib/python3/dist-packages/catkin_pkg/templates/metapackage.cmake.in")
