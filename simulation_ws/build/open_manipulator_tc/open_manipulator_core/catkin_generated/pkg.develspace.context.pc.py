# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/user/simulation_ws/devel/include;/home/user/simulation_ws/src/open_manipulator_tc/open_manipulator_core/include;/usr/include/eigen3".split(';') if "/home/user/simulation_ws/devel/include;/home/user/simulation_ws/src/open_manipulator_tc/open_manipulator_core/include;/usr/include/eigen3" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;std_msgs;sensor_msgs;geometry_msgs;moveit_msgs;moveit_core;moveit_ros_planning;moveit_ros_planning_interface;message_runtime".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "".split(';') if "" != "" else []
PROJECT_NAME = "open_manipulator_core"
PROJECT_SPACE_DIR = "/home/user/simulation_ws/devel"
PROJECT_VERSION = "0.0.0"
